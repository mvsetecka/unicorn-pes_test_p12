class AlignText
  
  def initialize(arguments)
    
    if arguments.size != 4
      abort("Nespravny pocet paramteru programu!")
    end
    
    @words = []
    @parsed_text = []
    @fname = arguments[0]
    @fname_out = arguments[1]
    @line_length = arguments[3].to_i
    abort("Zadana delka radku mimo podporovany rozsah") if @line_length > 200 || @line_length < 5
    parse_text_from_file(@fname)
    parse_new_lines(@line_length)
    add_spaces
    
    case arguments[2]
    when "L"
      save_file_L(@fname_out)
    when "R"
      save_file_R(@fname_out)
    when "C"
      save_file_C(@fname_out)
    when "J"
      save_file_J(@fname_out)
    else
      abort("Nespravny paramter programu - L/R/C/J")
    end
    
  end
  
  private
  
  def parse_text_from_file(fname)
    temp_words = []
    abort("Soubor #{fname} neexistuje!") if not File.exists?(fname)
    File.open(fname,"r") do |f|
      f.each do |line|
        
        if line != "\n"
          temp = line.split(" ")
          temp.each do |word|
            temp_words << word
          end
          @words << temp_words if f.eof?
        else
          @words << temp_words
          temp_words = []
        end
      end
   end
   
   def parse_new_lines(line_length)
   #funkce pro preformatovani textu podle nove delky radku
     @words.each do |paragraph|
       temp_line = []
       if not paragraph.empty?
        if get_array_length(paragraph) > line_length
       
          paragraph.each do |word|
            if temp_line.empty? && word.length > line_length
              @parsed_text << word
            
            elsif (get_array_length(temp_line) + word.length + 1) > line_length
              @parsed_text << temp_line
              temp_line = []
              temp_line << word
            else
              temp_line << word
            end
          end
          @parsed_text << temp_line
          
       else
        @parsed_text << paragraph
        
       end
      
      end
     @parsed_text << []
     end
     
   end
  end
  
   
   def add_spaces
    #projde pole a prida pred kazde slovo (krome prvniho na radku) jednu mezeru
    @parsed_text.each do |line|
        Array(line).each_with_index do |word, i|
          line[i] = " " + line[i] if i > 0
        end
      
    end
  end
   
   def get_array_length(arr, blanks=true)
    #funkce vrati delku vsech stringu v poli, pokud je druhy parametr false tak predpoklada, ze slova uz maji pred sebou mezery a nepridava je k celkove delce
     counter = 0
     Array(arr).each do |w|
       counter += w.length
     end
     counter += (arr.size - 1) if blanks == true
     return counter
   end
   
   def save_file_L(fname_out)
   #Ulozi do souboru text zarovnany doleva (pole @parsed_text bez uprav)
     File.open(fname_out,"w") do |f|
       @parsed_text.each do |line|
         Array(line).each do |word|
           f.print word
         end
         f.print "\n"
       end
     end
   end
   
   def save_file_R(fname_out)
   #Ulozi do souboru text zarovnany doleva (pole @parsed_text, pred kazdy radek pridame mezery (tolik mezer, kolik chybi do zadane delky radku))
     File.open(fname_out,"w") do |f|
       @parsed_text.each do |line|
       blanks = @line_length - get_array_length(line, false)
       blanks.times do
         f.print " "
       end
         Array(line).each do |word|
           f.print word
         end
         f.print "\n"
       end
     end
   end
   
   def save_file_C(fname_out)
   #Ulozi do souboru text zarovnany na stred
     File.open(fname_out,"w") do |f|
       @parsed_text.each do |line|
       blanks = @line_length - get_array_length(line, false)
       if blanks.even?
        blanks_L = blanks / 2
        blanks_R = blanks_L
       else
         blanks_L = blanks / 2
         blanks_R = blanks_L + 1
       end
       blanks_L.times do
         f.print " "
       end
         Array(line).each do |word|
           f.print word
         end
       blanks_R.times do
         f.print " "
       end
         f.print "\n"
       end
     end
   end
   
   def save_file_J(fname_out)
     #Ulozi do souboru text zarovnany do bloku, postupne pridava pred kazde slovo jednu mezeru dokud nedosahne zadane delky radku
     File.open(fname_out,"w") do |f|
       @parsed_text.each do |line|
       blanks = @line_length - get_array_length(line, false)
       
       if blanks < @line_length / 2 || Array(line).size > 3
        i = 0
        blanks.times do
          i = 0 if i >= line.size - 1
          i +=1
          line[i] = " " + line[i].to_s
        end
       end
         Array(line).each do |word|
           f.print word
         end
         f.print "\n"
       end
      
     
     end
   end
  
  
end

#Program na vstupu ocekava 4 parametry - input.txt output.txt L/R/C/J 80
# input.txt - vstupni soubor s textem
# output.txt - soubor pro zapsani zformatovaneho textu
# L/R/C/J - prave jedna z techto moznosti L- zarovnani doleva, R - zarovnani doprava, C - zarovnani na stred, J - zarovnani do bloku
# 80 - delka radku ve znacich, cislo mezi 5 a 200

#arguments = ARGV
arguments = ["2703.txt", "output.txt", "J", "55"]


AText = AlignText.new(arguments)
puts "----Hotovo, ulozeno----"
